module stash.bms.bz/bms/monitoringsystem

go 1.13

require (
	github.com/newrelic/go-agent v2.13.0+incompatible
	go.elastic.co/apm v1.9.0
	google.golang.org/grpc v1.26.0
)
